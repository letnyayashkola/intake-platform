PORT?=8000
SRC_DIR = intake_platform/
VERBOSITY?=2

init_env:
	ln -f dev/post-merge .git/hooks/
	./dev/create-dotenv.sh
	docker-compose -p intake_platform -f dev/docker-compose.yml up -d
	sleep 10
	poetry install --no-root --no-interaction
	poetry run ./manage.py migrate
	./dev/create-superuser.sh

start_env:
	docker-compose -p intake_platform -f dev/docker-compose.yml start

stop_env:
	docker-compose -p intake_platform -f dev/docker-compose.yml stop

django-run:
	set -a && \
	    poetry run python ./manage.py runserver ${PORT}

celery-run:
	poetry run /bin/sh -c './celery.sh'

django-shell:
	set -a && \
	    poetry run python ./manage.py shell

django-makemigrations:
	set -a && \
	    poetry run python ./manage.py makemigrations

django-applymigrations:
	set -a && \
	    poetry run python ./manage.py migrate

django-dbshell:
	set -a && \
	    poetry run python ./manage.py dbshell

django-collectstatic:
	set -a && \
	    poetry run python ./manage.py collectstatic

django-create-robot:
	set -a && \
	    poetry run python ./manage.py create_robot

poetry-shell:
	set -a && \
	    poetry shell

test-common:
	set -a && \
	    poetry run python ./manage.py test --verbosity ${VERBOSITY} intake_platform/common

test-intake:
	set -a && \
	    poetry run python ./manage.py test --verbosity ${VERBOSITY} intake_platform/intake

@test: test-common test-intake


#
# Linting
#

lint-isort:
	find ${SRC_DIR} -iname '*.py' -exec poetry run isort {} +

lint-black:
	poetry run black ${SRC_DIR}

lint-flake:
	poetry run flakehell lint

@lint: lint-isort lint-black lint-flake

.PHONY: @test
