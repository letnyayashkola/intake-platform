from django.conf import settings
from django.conf.urls import include

from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from intake_platform.common import views as common_views

from .views import IndexView, TestView, logout

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', IndexView.as_view(), name="index"),
    path('auth0-logout', logout, name='auth0-logout'),
    path('test/', TestView.as_view(), name="test"),
    # third-party stuff
    path('selectable/', include("selectable.urls")),
    path('accounts/profile/', common_views.profile, name="profile"),
    path('help/', common_views.help, name="help"),
    path('privacy/', common_views.privacy, name="privacy"),
    # intake
    path("", include(("intake_platform.intake.urls", "intake"), namespace="intake")),
    # curators
    path('', include(("intake_platform.intake.curators.urls", "curators"), namespace="curators"),),
    # guru
    path('', include(("intake_platform.intake.guru.urls", "guru"), namespace="guru")),
    # auth links
    path('', include('intake_platform.auth0login.social_auth_urls', namespace='social')),
    path('', include('django.contrib.auth.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
