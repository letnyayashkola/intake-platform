import json
from typing import Any, Dict, List, Tuple, Union

import redis
import requests
from annoying.functions import get_config


def get_redis_wp_select_key(is_adult: bool = True) -> str:
    if is_adult:
        return get_config('REDIS_KEY_WP_SELECT_ADULT')

    return get_config('REDIS_KEY_WP_SELECT_UNDERAGE')


def warm_up_wp_info_cache(fail_silently: bool = True, verbose: bool = False) -> None:
    BASE_URL = get_config('BASE_LSH_ORG_URL', 'https://letnyayashkola.org')
    urls = {
        'adult': f'{BASE_URL}/api/v1.0/workshops/',
        'underage': f'{BASE_URL}/api/v1.0/workshops/underage/',
    }

    data = {}
    for tag, url in urls.items():
        if verbose:
            print(tag, url)
        r = requests.get(url, verify=False, timeout=3)
        data[tag] = r.json()

    redis_url = get_config('REDIS_URL')
    redis_connection = redis.from_url(redis_url)

    if not fail_silently:
        assert redis_connection.ping(), f'Unable to ping redis server at {redis_url}'
    redis_keys = {
        'adult': get_redis_wp_select_key(is_adult=True),
        'underage': get_redis_wp_select_key(is_adult=False),
    }
    ttl = get_config('REDIS_WP_SELECT_TTL')

    for tag, wp_info in data.items():
        redis_key = redis_keys[tag]
        serialized_data = json.dumps(wp_info)
        redis_connection.set(redis_key, serialized_data, ex=ttl)
        if verbose:
            print(f'Saved data for {tag} at {redis_key} in {redis_url}')


def get_wp_cache(
    is_adult: bool = True, fail_silently: bool = True, verbose: bool = False,
) -> List[Dict[str, Any]]:
    """ Return data for underage and adult users """
    redis_url = get_config('REDIS_URL')
    redis_connection = redis.from_url(redis_url)

    if not fail_silently:
        assert redis_connection.ping(), f'Unable to ping redis server at {redis_url}'

    redis_key = get_redis_wp_select_key(is_adult=is_adult)
    tag = 'adult' if is_adult else 'underage'

    value = redis_connection.get(redis_key)
    if value is None:
        if verbose:
            print(f'Value for {tag} was not found in redis cache')
        return []

    data = json.loads(value.decode())
    return data


def _wp_cache_to_slug_name_mapping(wp_cache: List[Dict[str, Any]]) -> Dict[Tuple[str, str], str]:
    """ Transform cached dict to a lookup dictionary """

    mapping = {}
    for workshop in wp_cache:
        workshop_slug = workshop['slug']
        workshop_name = workshop['name']
        for program in workshop['programs']:
            program_slug = program['slug']
            program_name = program['name']

            mapping[workshop_slug, program_slug] = f'{workshop_name} ({program_name})'

    return mapping


def get_cached_full_wp_mapping(warm_up_cache: bool = False) -> Dict[Tuple[str, str], str]:
    if warm_up_cache:
        warm_up_wp_info_cache()
    cache_adult = get_wp_cache(is_adult=True)
    cache_underage = get_wp_cache(is_adult=False)
    mapping_adult = _wp_cache_to_slug_name_mapping(cache_adult)
    mapping_underage = _wp_cache_to_slug_name_mapping(cache_underage)

    full_mapping = {}
    full_mapping.update(mapping_adult)
    full_mapping.update(mapping_underage)

    return full_mapping


def store_fullname_in_redis(full_name: str, user_pk: int) -> None:
    """
    Stores fullname as redis list, with pk as a value.

    If list exists, redis will append to it

    full_name_key is assumed to be slugified and alphanumeric
    key prefix is specified in settings
    """
    redis_url = get_config('REDIS_URL')
    redis_connection = redis.from_url(redis_url)

    redis_key_prefix = get_config('REDIS_KEY_PREFIX_FULLNAMES')
    full_name_key = f'{redis_key_prefix}:{full_name}'

    redis_connection.sadd(full_name_key, user_pk)


def delete_user_fullname_in_redis(full_name: str, user_pk: int) -> None:
    redis_url = get_config('REDIS_URL')
    redis_connection = redis.from_url(redis_url)

    redis_key_prefix = get_config('REDIS_KEY_PREFIX_FULLNAMES')
    full_name_key = f'{redis_key_prefix}:{full_name}'

    redis_connection.srem(full_name_key, user_pk)
    if redis_connection.scard(full_name_key) == 0:
        redis_connection.delete(full_name_key)


def check_user_fullname_pk_in_redis(
    name_pk_pairs: List[Tuple[str, int]],
    drop_self: bool = True,
) -> Dict[str, List[int]]:
    """
    Check if fullnames are stored in redis as keys. Returns found pks
    O(len(name_pk_pairs)) complexity because each key requires its own check

    when drop_self is False, keep self pk in return list for all names
    """

    redis_url = get_config('REDIS_URL')
    redis_connection = redis.from_url(redis_url)

    redis_key_prefix = get_config('REDIS_KEY_PREFIX_FULLNAMES')

    clones_fullname_pk_pairs = {}
    for full_name, pk in name_pk_pairs:
        redis_key = f'{redis_key_prefix}:{full_name}'
        num_pks = redis_connection.scard(redis_key)
        pks = [int(pk) for pk in redis_connection.smembers(redis_key)]
        if drop_self:
            clones_fullname_pk_pairs[full_name] = sorted([
                clone_pk for clone_pk in pks if clone_pk != pk
            ])
        else:
            clones_fullname_pk_pairs[full_name] = sorted(pks)

    return clones_fullname_pk_pairs