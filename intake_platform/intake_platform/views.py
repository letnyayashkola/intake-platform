import json
from urllib.parse import urlencode

from annoying.functions import get_config
from django.conf import settings
from django.contrib.auth import logout as log_out
from django.contrib.auth.decorators import login_required
from django.dispatch import receiver
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.views.generic.base import TemplateView


def logout(request):
    log_out(request)
    return_to = urlencode({'returnTo': request.build_absolute_uri('/')})
    logout_url = 'https://%s/v2/logout?client_id=%s&%s' % (
        settings.SOCIAL_AUTH_AUTH0_DOMAIN,
        settings.SOCIAL_AUTH_AUTH0_KEY,
        return_to,
    )
    return HttpResponseRedirect(logout_url)


class IndexView(TemplateView):

    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        # context = get_intaking_workshops() ?
        context["intake_is_closed"] = get_config("INTAKE_IS_CLOSED", False)
        return context


class TestView(TemplateView):
    template_name = "test.html"

    def get_context_data(self, **kwargs):
        context = super(TestView, self).get_context_data(**kwargs)

        # URL = 'https://letnyayashkola.org/api/v1.0/workshops/'
        # context['workshops'] = requests.get(URL).json()

        context["dura_none"] = None
        context["dura_emptystr"] = ""
        context["dura_emptylst"] = []

        return context


def set_curators(sender, **kwargs):
    # TODO: fix this logic, different format, see common.views.profile
    # TODO: use this logic on corresponding enricher step
    user = kwargs.pop("user")
    curators = get_config("CURATORS", {})
    nabor_emails = get_config("NABOR_EMAILS", [])
    if user.email.endswith('@letnyayashkola.org'):
        prof = user.userprofile
        prof.is_curator = True
        prof.save()

    if user.email in curators:
        w_info = curators[user.email]
        w = w_info["workshop_slug"]
        p = w_info["program_slugs"] if "program_slugs" in w_info else []
        set_curator(user, w, p)

    if user.email in nabor_emails:
        prof = user.userprofile
        prof.is_nabor = True
        prof.save()
