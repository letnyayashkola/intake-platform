from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Div, Field, Layout, Submit
from django import forms

default_signup_header = Layout(
    Div(
        HTML(u"<h1 class='panel-title'>Карточка нового участника</h1>"),
        css_class="panel-heading text-center",
    ),
    HTML(u"<p class='text-center'>Уже регистрировались? <a href='{{ login_url }}'>Войдите</a></p>"),
)

default_signup_fields = Layout(
    Field("last_name", css_class="form-control"),
    Field("first_name", css_class="form-control"),
    Field("middle_name", css_class="form-control"),
    Field("is_male", css_class="form-control"),
    Field("location", css_class="form-control"),
    Field("birthdate", css_class="form-control"),
    Field("email", css_class="form-control"),
    Field("password1", css_class="form-control"),
)

default_signup_tail = Layout(
    HTML(
        u"{% if redirect_field_value %}<input type='hidden' name='{{ redirect_field_name }}' value='{{ redirect_field_value }}' />{% endif %}"
    ),
    FormActions(Submit("submit", u"Зарегистрироваться", css_class="btn btn-primary btn-block")),
)


class LoginForm:
    pass


class FancySignupForm:
    pass
