from social_core.exceptions import AuthCanceled
from social_django.middleware import SocialAuthExceptionMiddleware


class SilentSocialAuthMiddleware(SocialAuthExceptionMiddleware):
    def raise_exception(self, request, exception):
        return False

    def get_message(self, request, exception):
        if isinstance(exception, AuthCanceled):
            return 'Ты сам просил убавить звук и получил в ответ на это тишину!'
        return super(SilentSocialAuthMiddleware, self).get_message(request, exception)

    def get_redirect_uri(self, request, exception):
        if request.user.is_authenticated:
            if isinstance(exception, AuthCanceled):
                return '/'

        return super(SilentSocialAuthMiddleware, self).get_redirect_uri(request, exception)
