"""URLs module"""
from django.conf import settings
from django.urls import re_path as url
from social_core.utils import setting_name
from social_django import views as sa_views

from intake_platform.auth0login.views import complete

extra = getattr(settings, setting_name('TRAILING_SLASH'), True) and '/' or ''

app_name = 'social'

# Adapted from social_django.urls
urlpatterns = [
    # authentication / association
    url(r'^login/(?P<backend>[^/]+){0}$'.format(extra), sa_views.auth, name='begin'),
    url(r'^complete/(?P<backend>[^/]+){0}$'.format(extra), complete, name='complete'),
    # disconnection
    url(
        r'^disconnect/(?P<backend>[^/]+){0}$'.format(extra), sa_views.disconnect, name='disconnect'
    ),
    url(
        r'^disconnect/(?P<backend>[^/]+)/(?P<association_id>\d+){0}$'.format(extra),
        sa_views.disconnect,
        name='disconnect_individual',
    ),
]
