import pprint

from django.contrib.auth import get_user_model
from django.http import HttpResponseRedirect

from intake_platform.common.models import UserProfile


def create_profile(strategy, *args, **kwargs):
    if not kwargs['is_new']:
        return None

    user = kwargs['user']

    profile = UserProfile(user=user)
    profile.save()

    return None
