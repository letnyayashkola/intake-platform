from .assignments import AssignmentDownload, AssignmentMyResponseDownload, AssignmentResponse
from .first_step_views import (
    choose_my_way,
    collect_education_info,
    complete_first_step,
    demand_handymanship,
    fill_in_money,
    get_activity_info,
    leakage,
    parents,
    promise_help,
    provide_basic_info,
    provide_contacts,
    retrospect,
    upload_photo,
    wp_select,
)
from .secret_signups import (
    Secret105SignUp,
    SecretFRCSignUp,
    SecretFreestyleSignUp,
    SecretGenericSignUp,
    SecretHardOnGeo,
    SecretInclusioSignUp,
    SecretLawSignUp,
    SecretNeudobnoSignUp,
    SecretObrZhurSignUp,
    SecretRozetkaSignUp,
    SecretSciPubNauSignUp,
    SecretSignUp,
)
from .starting_test import check_user_not_dumb_actual_test, check_user_not_dumb_prequel
from .to_refactor import Messages, ReplacePhoto, WPCMotivationEdit, emails, workshop_programs
from .waiting_views import OwnAppReadOnly, ReadDeclineEmail, timeline
