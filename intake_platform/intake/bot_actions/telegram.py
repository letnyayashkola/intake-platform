from typing import Union, Optional

import requests
from annoying.functions import get_config


class Bot:
    BASE_URL = 'https://api.telegram.org'

    def __init__(self, token: Optional[str] = None, chat_id: Optional[int] = None) -> None:
        self.token: str = token or get_config('TELEGRAM_BOT_TOKEN')
        self.chat_id: Union[str, int] = chat_id or self.get_chat_url()

    @property
    def base_url(self) -> str:
        return f'{self.BASE_URL}/bot{self.token}'

    def sendMessage(
        self,
        text: str,
        parse_mode: str = 'Markdown',
        disable_notification: bool = True,
    ) -> None:
        payload = {
            'chat_id': self.chat_id,
            'text': text,
            'parse_mode': parse_mode,
            'disable_notification': disable_notification,
        }
        response = requests.post(
            f'{self.base_url}/sendMessage',
            json=payload,
            timeout=3
        )
        return response.json()

    @staticmethod
    def get_chat_url():
        # TODO: read from env vars
        # LSH_MAIN_CHAT_ID = -1001146790095
        LSH_MAIN_CHAT_ID = -1002066150429
        SHRIMPSIZEMOOSE_ID = 50921357
        if get_config('DEBUG', True):
            print('Using debug output')
            chat_id = SHRIMPSIZEMOOSE_ID
        else:
            chat_id = LSH_MAIN_CHAT_ID
        return chat_id
