from django.core.management.base import BaseCommand

from intake_platform.intake.models import ApplicationCase


class Command(BaseCommand):
    help = "Show slugified apps name and their cyrillic counterpart"

    def handle(self, *args, **options):
        for application in ApplicationCase.objects.all():
            print(application.pk, application.full_name_slugified)
