# coding: utf-8

from datetime import datetime

from annoying.functions import get_config
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from intake.models import ApplicationCase, UserBuck
from intake.tools import comment_on_case, filter_users_by_buck_slug


class Command(BaseCommand):
    help = "Makes dtworkshop notified for is_closed cases"

    def handle(self, *args, **options):
        # find all users who completed_first_step
        qs = ApplicationCase.objects.filter(is_closed=True)
        users = [a.user for a in qs]

        commenter_username = get_config("ROBOT_USERNAME", "admin")
        commenter = User.objects.get(username=commenter_username)

        counter = 0
        for u in filter_users_by_buck_slug(users, "passed_first_step"):
            buck = UserBuck.objects.get(user=u, slug="passed_first_step")
            case = ApplicationCase.objects.get(user=u)

            if case.datetime_workshop_notified:
                print("case #{} has dtwnotified already".format(case.pk))
            else:
                print("case #{} has been fixed!".format(case.pk))
                case.datetime_workshop_notified = buck.when
                comment_on_case(
                    case=case,
                    author=commenter,
                    comment="[auto] Время отправки в мастерскую было поправлено скриптом.",
                )
                case.short_comment = "fix_cases_workshop_notified command applied"
                case.last_modified = datetime.now()

                case.save()

                counter += 1

        print("total cases fixed:", counter)
