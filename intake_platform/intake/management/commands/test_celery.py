from django.core.management.base import BaseCommand

from intake_platform.intake.tasks import add


class Command(BaseCommand):
    help = "check celery task"

    def handle(self, *args, **options):
        result = add.delay(1, 2)
        print(result.get())
        result.forget()
