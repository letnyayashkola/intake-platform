from collections import Counter, defaultdict
from typing import Union

import requests
from annoying.functions import get_config
from django.core.management.base import BaseCommand

from intake_platform.intake.models import WorkshopPetitionConnection
from intake_platform.intake.tools import (
    get_workshop_programs,
    get_wp_accepted_apps,
    get_wp_declined_apps,
    get_wp_passed_apps,
    has_buck_after_buck,
    workshops_dict,
)

LSH_MAIN_CHAT_ID = -1001978074393
SHRIMPSIZEMOOSE_ID = 50921357
TOKEN = get_config('TELEGRAM_BOT_TOKEN')

if get_config('DEBUG', True):
    print('Using debug output')
    LSH_MAIN_CHAT_ID = SHRIMPSIZEMOOSE_ID


class Bot:
    BASE_URL = 'https://api.telegram.org'

    def __init__(self, token: str) -> None:
        self.token = token
        self.base_url = f'{self.BASE_URL}/bot{token}'

    def sendMessage(
        self,
        chat_id: Union[str, int],
        text: str,
        parse_mode: str = 'Markdown',
        disable_notification: bool = True,
    ) -> None:
        payload = {
            'chat_id': chat_id,
            'text': text,
            'parse_mode': parse_mode,
            'disable_notification': disable_notification,
        }
        r = requests.post(f'{self.base_url}/sendMessage', json=payload)
        return r.json()


def get_has_ass_count(apps_list):
    non_slackers = [x for x in apps_list if x.has_complete_homework()]
    return len([_ for _ in non_slackers])


class Command(BaseCommand):
    help = 'Writes ws counters to lsh chat'

    def handle(self, *args, **options):
        counts = defaultdict(int)
        passed_counts = defaultdict(int)

        ws_dict = workshops_dict()
        for w_slug, w_name in list(ws_dict.items()):
            wplist = get_workshop_programs(w_slug=w_slug, lean=True)
            for _, _, p_slug, p_name in wplist:
                wp_users = [
                    p.user
                    for p in WorkshopPetitionConnection.objects.filter(
                        workshop_slug=w_slug, program_slug=p_slug
                    )
                ]
                passed_counts[w_slug] += len(get_wp_passed_apps(w_slug, p_slug))
                passed = get_wp_passed_apps(w_slug, p_slug)
                counts[w_slug + '-passed'] += len(passed)
                counts[w_slug + '-has-ass'] += get_has_ass_count(passed)
                counts[w_slug + '-wp-accepted'] += len(get_wp_accepted_apps(w_slug, p_slug))
                counts[w_slug + '-wp-declined'] += len(get_wp_declined_apps(w_slug, p_slug))
                counts[w_slug + '-cunts'] += sum(
                    has_buck_after_buck(x, 'son_of_a_bitch', 'accepted_by_workshop')
                    for x in wp_users
                )

        ws_counts = ', \n'.join(
            [
                '  %s: %s[:%s] + *%s* + %s'
                % (
                    w,  # ws_dict[w],
                    counts[w + '-passed'],
                    counts[w + '-has-ass'],
                    counts[w + '-wp-accepted'],
                    counts[w + '-wp-declined'],
                )
                for w, c in sorted(
                    [t for t in iter(list(Counter(passed_counts).items())) if t[1] > 0],
                    key=lambda x: x[1],
                    reverse=True,
                )
            ]
        )

        total_accepted = sum(v for k, v in list(counts.items()) if k.endswith('-wp-accepted'))
        waiting = sum(v for k, v in list(counts.items()) if k.endswith('-has-ass'))

        mensaje = '\n'.join(
            [
                '`Заявки по мастерским`',
                '`(N_процесс[:N_прислали_задание] + N_принято + N_отказ)`',
                '',
                ws_counts,
                '',
                f'Всего принятых на сейчас: *{total_accepted}*',
                f'Ждут решения (с заданием): {waiting}',
            ]
        )
        Bot(TOKEN).sendMessage(LSH_MAIN_CHAT_ID, mensaje)
