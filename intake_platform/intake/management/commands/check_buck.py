# coding: utf-8

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from intake_platform.intake.models import UserBuck


class Command(BaseCommand):
    help = "Checks if the user has the buck"

    def add_arguments(self, parser):
        parser.add_argument("email", help="substring", type=str)
        parser.add_argument("buck", help="substring", type=str)

    def handle(self, *args, **options):
        users = User.objects.filter(email__icontains=options["email"])
        counter = 0
        for u in users:
            bucks = UserBuck.objects.filter(user=u, slug__icontains=options["buck"])

            if len(bucks) > 0:
                print("--- Found user ---")
                print(u.username)
                print("%s %s" % (u.first_name, u.last_name))
                print(u.email)
                for b in bucks:
                    print("--- Found BUCK ---")
                    print(b.slug)
                    print(b.when)
                    counter += 1

        print("total bucks found:", counter)
