from typing import Tuple, Union

import requests
from annoying.functions import get_config
from django.core.management.base import BaseCommand

from intake_platform.intake.models import UserBuck
from intake_platform.intake_platform.cache import get_cached_full_wp_mapping

LSH_MAIN_CHAT_ID = -1001978074393
SHRIMPSIZEMOOSE_ID = 50921357
TOKEN = get_config('TELEGRAM_BOT_TOKEN')

if get_config('DEBUG', True):
    print('Using debug output')
    LSH_MAIN_CHAT_ID = SHRIMPSIZEMOOSE_ID


class Bot:
    BASE_URL = 'https://api.telegram.org'

    def __init__(self, token: str) -> None:
        self.token = token
        self.base_url = f'{self.BASE_URL}/bot{token}'

    def sendMessage(
        self,
        chat_id: Union[str, int],
        text: str,
        parse_mode: str = 'Markdown',
        disable_notification: bool = True,
    ) -> None:
        payload = {
            'chat_id': chat_id,
            'text': text,
            'parse_mode': parse_mode,
            'disable_notification': disable_notification,
        }
        r = requests.post(f'{self.base_url}/sendMessage', json=payload)
        return r.json()


def get_intaking_wp_counts_str():
    r = requests.get('https://letnyayashkola.org/api/v1.0/workshop-programs/')
    data = r.json()
    intakers = len([wp for wp in data if wp['intaking']])
    total = len(data)
    return f'Продолжают набирать, программ: {intakers} (из {total})'


def get_intaking_wp_list():
    r = requests.get('https://letnyayashkola.org/api/v1.0/workshop-programs/', timeout=3)
    data = r.json()
    mapping = get_cached_full_wp_mapping(True)
    info_lines = []
    for intaking_wp in [wp for wp in data if wp['intaking']]:
        lookup_pair = intaking_wp['workshop-slug'], intaking_wp['slug']
        if lookup_pair not in mapping:
            info_lines.append(' '.join(lookup_pair))
        else:
            info_lines.append(mapping[lookup_pair])
    return '\n'.join(f'{index}. {line}' for index, line in enumerate(info_lines, start=1))


def count_bucks(buck_slug: str) -> int:
    return UserBuck.objects.filter(slug=buck_slug).count()


def get_accepted_females(denominator: int) -> Tuple[int, float]:
    accepted_users = [b.user for b in UserBuck.objects.filter(slug='accepted_by_workshop')]
    accepted_females = len([u for u in accepted_users if u.userprofile.is_female])

    females_perc: float = 0
    if accepted_females != 0:
        females_perc: float = accepted_females / denominator * 100

    return accepted_females, females_perc


class Command(BaseCommand):
    help = 'Pings orgs chat with basic counters'

    def handle(self, *args, **options):
        year: int = 2024

        total_apps: int = count_bucks('complete_first_step')
        passed_nabor: int = count_bucks('passed_first_step')
        declined_by_nabor: int = count_bucks('nabor_rejected')
        accepted_by_ws: int = count_bucks('accepted_by_workshop')
        declined_by_ws: int = count_bucks('declined_by_workshop')

        accepted_females, females_perc = get_accepted_females(denominator=accepted_by_ws)

        head: str = f'`Инфа по заявкам ЛШ-{year}`'
        total: str = f'Всего анкет: *{total_apps}*'
        nabor_info: str = ' '.join(
            [f'Прошло через Набор: *{passed_nabor}*', f'(Отказано набором: {declined_by_nabor})']
        )
        ws_info: str = '\n'.join(
            [
                f'Железно приняты мастерской: *{accepted_by_ws}*',
                f'из них {females_perc:.2g}% девочек {accepted_females}',
                f'Отвергнуты мастерской: {declined_by_ws}',
                '----',
                get_intaking_wp_counts_str(),
            ]
        )
        txt_bucks: str = '\n'.join([head, total, nabor_info, ws_info])

        bot = Bot(TOKEN)
        bot.sendMessage(LSH_MAIN_CHAT_ID, txt_bucks)
        # ws_list = f'Программы, ведущие набор:\n\n{get_intaking_wp_list()}'
        # bot.sendMessage(LSH_MAIN_CHAT_ID, ws_list)
