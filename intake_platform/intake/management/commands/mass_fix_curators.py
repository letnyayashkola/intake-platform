# coding: utf-8

from typing import Dict, List

from annoying.functions import get_config, get_object_or_None
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from intake_platform.intake.tools import set_curator


class Command(BaseCommand):
    help = 'Sets curators as curators and assigns them to corresponding programs'

    def handle(self, *args, **options):
        curators: Dict[str, List[str]] = get_config('CURATORS', {})
        new_curator_count: int = 0

        for email, ws_list in curators.items():
            try:
                user = get_object_or_None(User, email=email)
            except User.MultipleObjectsReturned:
                print(f'Something\'s wrong with email {email}')
                print('Multiple objects were returned when searching for user')
                continue
            if user is None:
                print(f'No such user yet ({email})')
                continue

            profile = user.userprofile
            print(f'Processing user {user} {email}')
            for workshop_slug in ws_list:
                set_curator(user, workshop_slug, programs=None)
                print(f'  Set as curator for {workshop_slug} (all programs)')

                if profile.is_curator:
                    continue

                print(f'>> Found undercover curator with email {email}')
                profile.is_curator = True
                profile.save()
                new_curator_count += 1

        print('Curators added:', new_curator_count)
