# coding: utf-8

from django.core.management.base import BaseCommand

from intake_platform.intake.models import UserBuck, WorkshopPetitionConnection
from intake_platform.intake.tools import get_workshop_programs, workshops_dict


class Command(BaseCommand):
    help = "Show various info and stats"
    IMPORTANT_LOCATION_THRESHOLD = 10

    def handle(self, *args, **options):
        for w_slug, w_name in workshops_dict().items():
            wplist = get_workshop_programs(w_slug=w_slug, lean=True)
            for _, _, p_slug, p_name in wplist:
                wp_users = [
                    p.user
                    for p in WorkshopPetitionConnection.objects.filter(
                        workshop_slug=w_slug, program_slug=p_slug
                    )
                ]
                passed_1st_step_count = UserBuck.objects.filter(
                    slug="passed_first_step", user__in=wp_users
                ).count()
                if passed_1st_step_count == 0:
                    print("%s/%s" % (w_slug, p_slug))
