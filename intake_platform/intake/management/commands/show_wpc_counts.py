# coding: utf-8

from datetime import datetime as dt
import collections

from django.core.management.base import BaseCommand

from intake_platform.intake.models import UserBuck, WorkshopPetitionConnection


class Command(BaseCommand):
    help = "Rename wpc attribution from workshop1/program1 to masterskaya2/programma2"

    def handle(self, *args, **options):
        slugs = (
            WorkshopPetitionConnection
            .objects
            .values_list('workshop_slug', 'program_slug')
            .all()
        )
        counts = collections.Counter(
            f'{workshop_slug}/{program_slug}'
            for workshop_slug, program_slug in slugs
        )
        for wpc, count in sorted(counts.items()):
            print(wpc, count)

