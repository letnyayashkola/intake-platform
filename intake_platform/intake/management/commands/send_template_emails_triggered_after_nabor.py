
from django.core.management.base import BaseCommand

from intake_platform.intake.models import (
     ApplicationCase,
     WorkshopProgramEmailTemplate,
)
from intake_platform.intake.tools import (
    has_buck,
    attach_buck,
    robot_comment_on_case,
    send_mail_to_applicant,
)


BUCK_SLUG = "email_template_passed_nabor_auto_sent"
DO_SEND_EMAILS = True
DO_STAMP_ROBOT_COMMENT = True
DO_ATTACH_BUCK = True


class Command(BaseCommand):
    help = "Send Workshop Programs Email templates"

    def handle(self, *args, **options):
        # find all users who completed_first_step
        qs = ApplicationCase.objects.filter(is_closed=True)
        tmpl_func = WorkshopProgramEmailTemplate.get_after_nabor_for_app

        # get users that don't have this trigger buck
        u_a_tmpl = filter(
            lambda x: not has_buck(x[0], BUCK_SLUG),
            filter(
                lambda x: x[-1] is not None,
                (
                    (
                        a.user,
                        a,
                        tmpl_func(a)
                    )
                    for a in qs
                )
            )
        )
        count_emails = 0
        count_robot_comments = 0
        count_attach_bucks = 0
        for u, a, t in u_a_tmpl:
            w, p = a.wp_names_tuple
            print(u, a, w, p)
            if DO_SEND_EMAILS:
                subject = f'ЛШ2024: вступительное задание ({w}::{p})'
                text = t.render()
                print("  sending email")
                send_mail_to_applicant(a, subject, text)
                count_emails += 1
            if DO_STAMP_ROBOT_COMMENT:
                print("  commenting on the app")
                robot_comment = 'На выбранной программе участнику уходит письмо при прохождении набора. Оно ушло участнику.'
                robot_comment_on_case(case=a, comment=robot_comment)
                count_robot_comments += 1
            if DO_ATTACH_BUCK:
                print("  attaching buck", BUCK_SLUG)
                if not attach_buck(a.user, BUCK_SLUG):
                    print("  this failed though")
                else:
                    count_attach_bucks += 1

        print("== IN TOTAL ==")
        print("Sent", count_emails, "emails")
        print("Stamped", count_robot_comments, "robot comments")
        print("Attached", count_attach_bucks, "bucks")
