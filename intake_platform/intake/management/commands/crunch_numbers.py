# coding: utf-8

from datetime import datetime

import openpyxl
from django.core.management.base import BaseCommand
from openpyxl.cell import get_column_letter

from common.models import UserProfile
from intake.models import User, UserEducationInfo


class Command(BaseCommand):
    help = "Show various info and stats"
    IMPORTANT_LOCATION_THRESHOLD = 0

    def get_edu_info(self, users):
        """ Returns an info dict with education and degrees info on said users

            Sample return:
                {
                    'finished_eductaion': {
                        'accepted_by_workshop': {2: 1, 5: 2},
                        'passed_first_step': {1: 4, 2: 6, 3: 4, 4: 85, 5: 46}
                    },
                    'degrees': {
                        'accepted_by_workshop': {1: 1},
                        'passed_first_step': {1: 9, 2: 33, 4: 1}
                    },
                }
        """

        ued_stats = {b: {} for b in users.keys()}
        deg_stats = {b: {} for b in users.keys()}

        for s, u_list in users.items():
            for u in u_list:
                ued = UserEducationInfo.objects.get(user=u)
                finished_ed = ued.finished_ed

                if finished_ed not in ued_stats[s]:
                    ued_stats[s][finished_ed] = 1
                else:
                    ued_stats[s][finished_ed] += 1

                # if ued.degree_ed:
                #    deg = ued.degree_ed
                #    if deg not in deg_stats[s]:
                #        deg_stats[s][deg] = 1
                #    else:
                #        deg_stats[s][deg] += 1

        return {"finished_education": ued_stats, "degrees": deg_stats}

    def get_ages_tuple(self, users):
        """ Returns a tuple of ages info on the input users:
                (minimal age, maximal age, average age, weights)

                first two numbers are integer,
                average age is float
                weights is a count dict {age: count_on_users}
        """

        ages = [u.age for u in UserProfile.objects.filter(user__in=users)]

        mn = min(ages)
        mx = max(ages)
        avg = float(sum(ages)) / len(ages)

        weights = {}
        for a in ages:
            weights[a] = (weights[a] + 1) if a in weights else 1

        return mn, mx, avg, weights

    def get_distinct_locations_info(self, users):
        """ Get location stats info on the given users
        """

        locations = [u.location.lower().strip() for u in UserProfile.objects.filter(user__in=users)]

        loc_dict = {}
        for loc in locations:
            loc_dict[loc] = (loc_dict[loc] + 1) if loc in loc_dict else 1

        return locations, loc_dict

    def handle(self, *args, **options):
        # Find users with buck passed

        buck_slugs_to_check = ["passed_first_step", "accepted_by_workshop"]

        users = {b: [] for b in buck_slugs_to_check}
        for b in buck_slugs_to_check:
            users[b] = list(User.objects.filter(userbuck__slug=b).all())

        print("Education info")
        ued_info = self.get_edu_info(users)
        ued_stats = ued_info["finished_education"]
        deg_stats = ued_info["degrees"]

        # display edu stats
        for s in buck_slugs_to_check:
            print(("[ %s ]" % s).center(30, "="))

            print("Образование")
            for k, v in ued_stats[s].items():
                print(k, v)
                print("  %i: %s -> %i" % (k, UserEducationInfo.EDU_CHOICES_2016[k - 201600][1], v))

            # print u"Степень"
            # for k, v in deg_stats[s].items():
            #    print "  %i: %s -> %i" % (k, UserEducationInfo.DEGREE_CHOICES[k][1], v)

        # pprint.pprint(deg_stats)

        print("Age, Location info")
        for s, u_list in users.items():
            print(("[ %s ]" % s).center(30, "="))
            age_mn, age_mx, age_avg, weights = self.get_ages_tuple(u_list)
            print("Ages: MIN %g, MAX %g, AVG %g" % (age_mn, age_mx, age_avg))

            locations, l_dict = self.get_distinct_locations_info(u_list)
            print("Всего различных локаций: %s" % len([k for k in l_dict.keys()]))
            print("Популярные локации (больше %s заявок):" % self.IMPORTANT_LOCATION_THRESHOLD)
            for l, v in l_dict.items():
                if v > self.IMPORTANT_LOCATION_THRESHOLD:
                    print("  %s (%s)" % (l, v))

            print("Дубна: ", l_dict["дубна"])

        ###################
        now_str = datetime.now().strftime("%Y%b%d").lower()
        fname = "locations_%s.xslx" % now_str

        workbook = openpyxl.Workbook()  # outfile, {'in_memory': True})
        worksheet = workbook.get_active_sheet()

        for col_num in [1, 2, 3, 4, 6, 7, 8, 9]:
            c = worksheet.cell(row=1, column=col_num)
            c.style.font.bold = True
            column_letter = get_column_letter(col_num)
            worksheet.column_dimensions[column_letter].width = 20

        col_num = 5  # Location column
        c = worksheet.cell(row=1, column=col_num)
        c.style.font.bold = True
        column_letter = get_column_letter(col_num)
        worksheet.column_dimensions[column_letter].width = 80

        col_num = 6  # Email?
        c = worksheet.cell(row=1, column=col_num)
        c.style.font.bold = True
        column_letter = get_column_letter(col_num)
        worksheet.column_dimensions[column_letter].width = 100

        col_num = 3  # Education column
        c = worksheet.cell(row=1, column=col_num)
        c.style.font.bold = True
        column_letter = get_column_letter(col_num)
        worksheet.column_dimensions[column_letter].width = 120

        def foo(a):
            for k, v in a.full_info().items():
                if k in ["location", "birthdate"]:
                    yield v
                elif k == "uei":
                    yield v.get_finished_ed_display()
                    yield v.affiliation

        for i, (s, u_list) in enumerate(iter(users.items()), start=1):
            locations, l_dict = self.get_distinct_locations_info(u_list)
            for k, v in l_dict.items():
                c = worksheet.cell(row=i, column=1)
                c.value = k
                c.style.alignment.wrap_text = True
                c = worksheet.cell(row=i, column=2)
                c.value = v
                c.style.alignment.wrap_text = True

        workbook.save(fname)
        return fname
