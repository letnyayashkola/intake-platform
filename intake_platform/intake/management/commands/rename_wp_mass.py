# coding: utf-8

from datetime import datetime as dt
import collections

from django.core.management.base import BaseCommand

from intake_platform.intake.models import UserBuck, WorkshopPetitionConnection


class Command(BaseCommand):
    help = "Rename wpc attribution from workshop1/program1 to masterskaya2/programma2"

    def add_arguments(self, parser):
        parser.add_argument('from-wpc', type=str)
        parser.add_argument('to-wpc', type=str)

    def handle(self, *args, **options):
        lhs_w, lhs_p = options['from-wpc'].split('/')
        rhs_w, rhs_p = options['to-wpc'].split('/')
        wpcs = (
            WorkshopPetitionConnection
            .objects
            .filter(workshop_slug=lhs_w, program_slug=lhs_p)
            .values_list('pk', flat=True)
            .all()
        )
        confirm = input(f'Change {len(wpcs)} wpcs {lhs_w}/{lhs_p} -> {rhs_w}/{rhs_p}? (Y/N) ')
        if not confirm.startswith('Y'):
            print('Bailing out, not assertive')
        else:
            update = (
                WorkshopPetitionConnection
                .objects
                .filter(pk__in=wpcs)
                .update(workshop_slug=rhs_w, program_slug=rhs_p)
            )
            print(f'Renamed {update} wpcs')
