# coding: utf-8

from datetime import datetime as dt

from django.core.mail import mail_admins
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Send test email to SERVER_ADMIN"

    def handle(self, *args, **options):
        year = dt.now().year
        subject = f"ЛШ{year}: тестирование почты"
        text = """Добрый день.

Ит факин лукс лайк почта из факин ворикин

Всего доброго
Сара Коннор
"""

        mail_admins(subject, text)
