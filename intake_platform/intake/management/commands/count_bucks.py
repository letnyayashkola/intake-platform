# coding: utf-8

from django.core.management.base import BaseCommand

from intake_platform.common.models import UserProfile
from intake_platform.intake.models import UserBuck


class Command(BaseCommand):
    help = "Shows how many bucks exist currently"

    def handle(self, *args, **options):
        users = UserProfile.objects.all()
        print("Total users: ", users.count())
        distinct_slugs = UserBuck.objects.values_list("slug", flat=True).distinct()
        print("Found {} slugs".format(len(distinct_slugs)))
        for slug in distinct_slugs:
            print(slug, UserBuck.objects.filter(slug=slug).count())
