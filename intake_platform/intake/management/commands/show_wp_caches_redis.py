from django.core.management.base import BaseCommand

from intake_platform.intake_platform.cache import get_wp_cache


class Command(BaseCommand):
    help = "Tell what's in wp cache"

    def handle(self, *args, **options):
        for tag, is_adult in [('underage', False), ('adult', True)]:
            data = get_wp_cache(is_adult=is_adult, verbose=True, fail_silently=False)
            print(tag, 'wp info is for the following slugs:')
            for entry in data:
                print(" ", entry['slug'])
                for program in entry['programs']:
                    underage_friendly_str = '(minors'
                    if program['accepts_underage']:
                        underage_friendly_str += ' accepted)'
                    else:
                        underage_friendly_str += ' are NOT allowed)'
                    print("   ", program['slug'], underage_friendly_str)
