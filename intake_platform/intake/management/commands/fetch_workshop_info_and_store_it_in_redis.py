from django.core.management.base import BaseCommand

from intake_platform.intake_platform.cache import warm_up_wp_info_cache


class Command(BaseCommand):
    help = 'Warm up wp info cache'

    def handle(self, *args, **options):
        warm_up_wp_info_cache(fail_silently=False, verbose=True)
