import redis

from annoying.functions import get_config

from django.core.management.base import BaseCommand

from intake_platform.intake.models import ApplicationCase


class Command(BaseCommand):
    help = "Show slugified apps name and their cyrillic counterpart"
    def handle(self, *args, **options):
        redis_url = get_config('REDIS_URL')
        redis_connection = redis.from_url(redis_url)

        redis_key_prefix = get_config('REDIS_KEY_PREFIX_FULLNAMES')

        for application in ApplicationCase.objects.all():
            full_name = application.full_name_slugified
            redis_key = f'{redis_key_prefix}:{full_name}'
            user_pk = application.user.pk
            redis_connection.sadd(redis_key, user_pk)
            print(f'stored {full_name}->{user_pk}')
