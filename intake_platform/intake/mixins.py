from .decorators import curator_restricted, guru_restricted, intake_restricted, super_nabor_restricted


class NaborRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(NaborRequiredMixin, cls).as_view(**initkwargs)
        # return intake_restricted(view)
        return intake_restricted()(view)


class CuratorRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(CuratorRequiredMixin, cls).as_view(**initkwargs)
        # return intake_restricted(view)
        return curator_restricted()(view)


class GuruRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(GuruRequiredMixin, cls).as_view(**initkwargs)
        # return guru_restricted(view)
        return guru_restricted()(view)


class SuperNaborRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(SuperNaborRequiredMixin, cls).as_view(**initkwargs)
        # return guru_restricted(view)
        return super_nabor_restricted()(view)

# vim: set ts=4 sw=4 sts=4 et :
