from functools import wraps

from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect, resolve_url

from intake_platform.common.tools import get_user
from intake_platform.intake.tools import has_buck


class CONSTS(object):
    no_rights_message = (u"У вас недостаточно прав для доступа к этому разделу",)
    completed_step_message = u"вы уже выполняли этот шаг."


def user_has_no_buck_with_slug(buck_slug, redirect_url=None):
    """
    Decorator for views that checks if the user has NO buck with the requested
    slug. If he hasn't, redirect to REDIRECT_LOGIN_URL or redirect_url provided.
    """

    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            user = get_user(request)
            if not has_buck(user, buck_slug):
                return view_func(request, *args, **kwargs)
            resolved_url = resolve_url(redirect_url or settings.LOGIN_REDIRECT_URL)

            messages.info(request, u"Операция недоступна: %s" % CONSTS.completed_step_message)
            return redirect(resolved_url)

        return _wrapped_view

    return decorator


# С этим декоратором что-то не так и его приходится юзать как 'name()'
def intake_restricted(redirect_url=None):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            user = get_user(request)
            if user.is_authenticated and user.userprofile.is_nabor:
                return view_func(request, *args, **kwargs)
            resolved_url = resolve_url(redirect_url or settings.LOGIN_REDIRECT_URL)

            messages.error(request, u"Доступ запрещён: %s" % CONSTS.no_rights_message)
            return redirect(resolved_url)

        return _wrapped_view

    return decorator


# С этим декоратором что-то не так и его приходится юзать как 'name()'
# FIXME: нарушает DRY, так как слизан с intake_restricted
def curator_restricted(redirect_url=None):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            user = get_user(request)
            if user.is_authenticated and user.userprofile.is_curator:
                return view_func(request, *args, **kwargs)
            resolved_url = resolve_url(redirect_url or settings.LOGIN_REDIRECT_URL)

            messages.error(request, u"Доступ запрещён: %s" % CONSTS.no_rights_message)
            return redirect(resolved_url)

        return _wrapped_view

    return decorator


def guru_restricted(redirect_url=None):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            user = get_user(request)
            if user.is_authenticated and user.userprofile.is_guru:
                return view_func(request, *args, **kwargs)
            resolved_url = resolve_url(redirect_url or settings.LOGIN_REDIRECT_URL)

            messages.error(request, u"Доступ запрещён: %s" % CONSTS.no_rights_message)
            return redirect(resolved_url)

        return _wrapped_view

    return decorator


def super_nabor_restricted(redirect_url=None):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            user = get_user(request)
            if user.is_authenticated and user.userprofile.is_super_nabor:
                return view_func(request, *args, **kwargs)
            resolved_url = resolve_url(redirect_url or settings.LOGIN_REDIRECT_URL)

            messages.error(request, u"Доступ запрещён: %s" % CONSTS.no_rights_message)
            return redirect(resolved_url)

        return _wrapped_view

    return decorator

# vim: set ts=4 sw=4 sts=4 et :
