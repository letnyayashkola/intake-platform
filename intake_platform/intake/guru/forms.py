# coding: utf-8

from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Field, Layout, Submit  # Div, Hidden
from django import forms

from ..models import WorkshopProgramMetaInfo


class WPSelectMultipleForm(forms.Form):
    programs = forms.ModelMultipleChoiceField(
        queryset=WorkshopProgramMetaInfo.objects.all(), widget=forms.widgets.CheckboxSelectMultiple
    )
    user_pk = forms.IntegerField(widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        super(WPSelectMultipleForm, self).__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Field("programs"),
            Field("user_pk"),
            FormActions(
                Submit(
                    "submit",
                    u"Сделать куратором выбранных направлений",
                    css_class="btn btn-primary",
                ),
                HTML(
                    u"""
                     {% for url, caption in back_urls %}
                     <a class="btn btn-default" href="{{ url }}">{{ caption }}</a>
                     {% endfor %}
                     """
                ),
            ),
        )
        self.helper = helper


class BlackListUploadForm(forms.Form):
    xlsx = forms.FileField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Field('xlsx'),
            FormActions(Submit('submit', 'Загрузить ёпт', css_class='btn btn-primary',),),
        )
        self.helper = helper


class BotActionSelectForm(forms.Form):
    bot_action = forms.ChoiceField(
        choices=[(0, 'short_apps_to_lsh_chat')]
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        helper = FormHelper(self)
        helper.layout = Layout(
            Field("bot_action"),
            FormActions(Submit("submit", "PEW-PEW", css_class="btn btn-primary")),
        )
        self.helper = helper
