import pytz
from datetime import datetime as dt

import redis
from annoying.functions import get_config

from django.test import TestCase
from django.utils.timezone import make_aware
from django.contrib.auth import get_user_model

from intake_platform.intake.models import ApplicationCase
from intake_platform.common.models import UserProfile
from intake_platform.intake_platform.cache import check_user_fullname_pk_in_redis, delete_user_fullname_in_redis


class RedisTestCaseWithCleanup(TestCase):
    def tearDown(self):
        profile = UserProfile.objects.get(user__email='fake@email.com')
        delete_user_fullname_in_redis(profile.full_name_slugified, profile.user.pk)


class ApplicationCaseCanCreate(RedisTestCaseWithCleanup):
    def setUp(self):
        user = get_user_model().objects.create(
            first_name='Григорий',
            last_name='Смаркоходов',
            email='fake@email.com',
        )
        UserProfile.objects.create(
            user=user,
            middle_name='Валентинович',
            birthdate=dt(2000, 1, 1),
            location='Somewhere on the Earth',
        )
        ApplicationCase.objects.create(
            user=user,
            created=make_aware(dt(2020, 1, 20)),
            short_comment='This is a test case',
        )

    def test_case_can_be_found_using_email(self):
        self.assertTrue(
            ApplicationCase.objects.filter(user__email='fake@email.com').exists()
        )


class ApplicationCaseCanSpotFullNameMatch(RedisTestCaseWithCleanup):
    def setUp(self):
        user = get_user_model().objects.create(
            first_name='Григорий',
            last_name='Смаркоходов',
            email='fake@email.com',
        )
        UserProfile.objects.create(
            user=user,
            middle_name='Валентинович',
            birthdate=dt(2000, 1, 1, tzinfo=pytz.UTC),
            location='Somewhere on the Earth',
        )
        ApplicationCase.objects.create(
            user=user,
            created=make_aware(dt(2020, 1, 21)),
            short_comment='This is a test case',
        )

    def test_saved_application_has_name_stored_in_redis_already(self):
        case = ApplicationCase.objects.get(user__email='fake@email.com')

        got = check_user_fullname_pk_in_redis(
            [(case.full_name_slugified, case.pk)], drop_self=False,
        )

        expected = {case.full_name_slugified: [case.pk]}
        self.assertEqual(got, expected)


class ApplicationCaseCanDetectDuplicates(TestCase):
    def setUp(self):
        user1 = get_user_model().objects.create(
            username='fake_gregory1',
            first_name='Григорий',
            last_name='Смаркоходов',
            email='fake1@email.com',
        )
        UserProfile.objects.create(
            user=user1,
            middle_name='Валентинович',
            birthdate="2000-01-01",
            location='Somewhere on the Earth',
        )
        ApplicationCase.objects.create(
            user=user1,
            created=make_aware(dt(2020, 1, 22)),
            short_comment='This is a test case fake1',
        )

        user2 = get_user_model().objects.create(
            username='fake_gregory2',
            first_name='Григорий',
            last_name='Смаркоходов',
            email='fake2@email.com',
        )
        UserProfile.objects.create(
            user=user2,
            middle_name='Валентинович',
            birthdate="2000-01-01",
            location='Somewhere on the Earth',
        )
        ApplicationCase.objects.create(
            user=user2,
            created=make_aware(dt(2020, 1, 23)),
            short_comment='This is a test case fake2',
        )


    def test_two_cases_have_different_pks(self):
        case1 = ApplicationCase.objects.get(user__email='fake1@email.com')
        case2 = ApplicationCase.objects.get(user__email='fake2@email.com')

        self.assertNotEqual(case1.pk, case2.pk)

    def test_two_cases_have_same_slug_fullname(self):
        case1 = ApplicationCase.objects.get(user__email='fake1@email.com')
        case2 = ApplicationCase.objects.get(user__email='fake2@email.com')

        self.assertEqual(case1.full_name_slugified, case2.full_name_slugified)

    def test_case_can_detect_duplicates(self):
        case1 = ApplicationCase.objects.get(user__email='fake1@email.com')
        case2 = ApplicationCase.objects.get(user__email='fake2@email.com')

        case1_clones = case1._ApplicationCase__fetch_other_users_with_same_full_name()
        self.assertEqual(case1_clones, [case2.user.pk])
        self.assertTrue(case1.has_clones)

        case2_clones = case2._ApplicationCase__fetch_other_users_with_same_full_name()
        self.assertEqual(case2_clones, [case1.user.pk])
        self.assertTrue(case2.has_clones)

    def test_case_can_get_duplicate(self):
        case1 = ApplicationCase.objects.get(user__email='fake1@email.com')
        case2 = ApplicationCase.objects.get(user__email='fake2@email.com')

        case1_clones = case1.fetch_clone_cases()
        self.assertEqual(len(case1_clones), 1)
        self.assertTrue(isinstance(case1_clones[0], ApplicationCase))
        self.assertEqual(list(case1_clones), [case2])
        self.assertEqual([case.pk for case in case1_clones], [case2.pk])

        case2_clones = case2.fetch_clone_cases()
        self.assertEqual(len(case2_clones), 1)
        self.assertTrue(isinstance(case2_clones[0], ApplicationCase))
        self.assertEqual(list(case2_clones), [case1])
        self.assertEqual([case.pk for case in case2_clones], [case1.pk])

    def tearDown(self):
        for email_login in ['fake1', 'fake2']:
            profile = UserProfile.objects.get(user__email=f'{email_login}@email.com')
            delete_user_fullname_in_redis(profile.full_name_slugified, profile.user.pk)
