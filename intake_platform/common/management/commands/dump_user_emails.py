from django.core.management.base import BaseCommand

from common.models import User


class Command(BaseCommand):
    help = "Dump emails of all registered users. ALL."

    def handle(self, *args, **options):
        for u in User.objects.values("email"):
            print(u["email"])
