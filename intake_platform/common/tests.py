import pytz
from datetime import datetime as dt

import redis

from annoying.functions import get_config
from django.contrib.auth import get_user_model
from django.test import TestCase

from intake_platform.intake.models import ApplicationCase
from intake_platform.common.models import UserProfile
from intake_platform.intake_platform.cache import check_user_fullname_pk_in_redis, delete_user_fullname_in_redis


class RedisTestCaseWithCleanup(TestCase):
    def tearDown(self):
        profile = UserProfile.objects.get(user__email='fake@email.com')
        delete_user_fullname_in_redis(profile.full_name_slugified, profile.user.pk)


class UserProfileTestIncompleteProfile(RedisTestCaseWithCleanup):
    def setUp(self):
        user = get_user_model().objects.create(
            first_name='Григорий',
            last_name='Смаркоходов',
            email='fake@email.com',
        )
        UserProfile.objects.create(
            user=user,
            middle_name='Валентинович',
        )

    def test_profile_has_all_basic_fields(self):
        profile = UserProfile.objects.get(user__email='fake@email.com')

        self.assertFalse(profile.has_all_basic_fields())


class UserProfileTestCompleteProfile(RedisTestCaseWithCleanup):
    def setUp(self):
        user = get_user_model().objects.create(
            first_name='Григорий',
            last_name='Смаркоходов',
            email='fake@email.com',
        )
        UserProfile.objects.create(
            user=user,
            middle_name='Валентинович',
            birthdate=dt(2000, 1, 1, tzinfo=pytz.UTC),
            location='Somewhere on the Earth',
        )

    def test_profile_has_all_basic_fields(self):
        profile = UserProfile.objects.get(user__email='fake@email.com')

        self.assertTrue(profile.has_all_basic_fields())


class UserProfileTestAgeStuff(RedisTestCaseWithCleanup):
    def setUp(self):
        user = get_user_model().objects.create(
            first_name='Григорий',
            last_name='Смаркоходов',
            email='fake@email.com',
        )
        UserProfile.objects.create(
            user=user,
            middle_name='Валентинович',
            birthdate=dt(2000, 1, 1, tzinfo=pytz.UTC),
        )

    def test_profile_not_underage(self):
        profile = UserProfile.objects.get(user__email='fake@email.com')

        self.assertFalse(profile.is_underage)

    def test_profile_is_adult_when_needed(self):
        profile = UserProfile.objects.get(user__email='fake@email.com')

        with self.settings(LSH_DATES_DICT={'start': (2020, 1, 1)}):
            self.assertTrue(profile.is_adult)

        with self.settings(LSH_DATES_DICT={'start': (2010, 1, 1)}):
            self.assertFalse(profile.is_adult)

    def test_profile_not_underage(self):
        profile = UserProfile.objects.get(user__email='fake@email.com')

        self.assertFalse(profile.is_underage)


class UserProfileSlugifiedFullname(RedisTestCaseWithCleanup):
    def setUp(self):
        user = get_user_model().objects.create(
            first_name='Some',
            last_name='Unique',
            email='fake@email.com',
        )
        UserProfile.objects.create(
            user=user,
            middle_name='Legendary',
            birthdate=dt(2000, 1, 1, tzinfo=pytz.UTC),
        )

    def test_profile_fullname_slugified_match_expectation(self):
        profile = UserProfile.objects.get(user__email='fake@email.com')
        redis_key_prefix = get_config('REDIS_KEY_PREFIX_FULLNAMES')
        redis_key = f'{redis_key_prefix}:{profile.full_name_slugified}'
        redis_url = get_config('REDIS_URL')
        redis_connection = redis.from_url(redis_url)

        self.assertTrue(redis_connection.exists(redis_key))
        self.assertEqual(redis_connection.scard(redis_key), 1)

    def tearDown(self):
        profile = UserProfile.objects.get(user__email='fake@email.com')
        delete_user_fullname_in_redis(profile.full_name_slugified, profile.user.pk)


class UserProfileStoresPkInRedis(RedisTestCaseWithCleanup):
    def setUp(self):
        user = get_user_model().objects.create(
            first_name='Григорий',
            last_name='Аркоходов',
            email='fake@email.com',
        )
        UserProfile.objects.create(
            user=user,
            middle_name='Валентинович',
            birthdate=dt(2000, 1, 1, tzinfo=pytz.UTC),
        )
    def test_profile_has_full_name_stored_in_redis(self):
        profile = UserProfile.objects.get(user__email='fake@email.com')

        got = check_user_fullname_pk_in_redis(
            [
                (profile.full_name_slugified, profile.user.pk)
            ]
        )

        expected = {profile.full_name_slugified: []}
        self.assertEqual(got, expected)


class UserProfileSlugifiedFullnameNoMiddlename(RedisTestCaseWithCleanup):
    def setUp(self):
        user = get_user_model().objects.create(
            first_name='Григорий',
            last_name='Аркоходов',
            email='fake@email.com',
        )
        UserProfile.objects.create(
            user=user,
            birthdate=dt(2000, 1, 1, tzinfo=pytz.UTC),
        )

    def test_profile_fullname_slugified_match_expectation(self):
        profile = UserProfile.objects.get(user__email='fake@email.com')

        self.assertEqual(
            profile.full_name_slugified,
            'arkokhodov-grigorii',
        )


class UserProfilesMultipleCheckRedisReturns(TestCase):
    def setUp(self):
        user1 = get_user_model().objects.create(
            username='fake1',
            first_name='first1',
            last_name='last1',
            email='fake1@email.com',
        )
        UserProfile.objects.create(
            user=user1,
            middle_name='middle1',
            birthdate=dt(2000, 1, 1, tzinfo=pytz.UTC),
        )

        user2 = get_user_model().objects.create(
            username='fake2',
            first_name='first2',
            last_name='last2',
            email='fake2@email.com',
        )
        UserProfile.objects.create(
            user=user2,
            middle_name='middle2',
            birthdate=dt(2000, 1, 1, tzinfo=pytz.UTC),
        )

        user3 = get_user_model().objects.create(
            username='fake3',
            first_name='first3',
            last_name='last3',
            email='fake3@email.com',
        )
        UserProfile.objects.create(
            user=user3,
            middle_name='middle3',
            birthdate=dt(2000, 1, 1, tzinfo=pytz.UTC),
        )

    def test_profiles_different_names_no_duplicates(self):
        p1 = UserProfile.objects.get(user__email='fake1@email.com')
        p2 = UserProfile.objects.get(user__email='fake2@email.com')
        p3 = UserProfile.objects.get(user__email='fake3@email.com')

        got = check_user_fullname_pk_in_redis(
            [
                (p1.full_name_slugified, p1.pk),
                (p2.full_name_slugified, p2.pk),
                (p3.full_name_slugified, p3.pk),
            ]
        )

        expected = {
            p1.full_name_slugified: [],
            p2.full_name_slugified: [],
            p3.full_name_slugified: [],
        }
        self.assertEqual(got, expected)

    def tearDown(self):
        for email_login in ['fake1', 'fake2', 'fake3']:
            profile = UserProfile.objects.get(user__email=f'{email_login}@email.com')
            delete_user_fullname_in_redis(profile.full_name_slugified, profile.user.pk)



class UserProfilesCheckDuplicatesFound(TestCase):
    def setUp(self):
        user1 = get_user_model().objects.create(
            username='fake1',
            first_name='first',
            last_name='last',
            email='fake1@email.com',
        )
        UserProfile.objects.create(
            user=user1,
            middle_name='middle',
            birthdate=dt(2000, 1, 1, tzinfo=pytz.UTC),
        )

        user2 = get_user_model().objects.create(
            username='fake2',
            first_name='first',
            last_name='last',
            email='fake2@email.com',
        )
        UserProfile.objects.create(
            user=user2,
            middle_name='middle',
            birthdate=dt(2000, 1, 1, tzinfo=pytz.UTC),
        )

        user3 = get_user_model().objects.create(
            username='fake3',
            first_name='first',
            last_name='last',
            email='fake3@email.com',
        )
        UserProfile.objects.create(
            user=user3,
            middle_name='middle',
            birthdate=dt(2000, 1, 1, tzinfo=pytz.UTC),
        )

    def test_profiles_same_name_list_duplicates(self):
        p1 = UserProfile.objects.get(user__email='fake1@email.com')
        p2 = UserProfile.objects.get(user__email='fake2@email.com')
        p3 = UserProfile.objects.get(user__email='fake3@email.com')

        got = check_user_fullname_pk_in_redis(
            [
                (p1.full_name_slugified, p1.pk),
                (p2.full_name_slugified, p2.pk),
                (p3.full_name_slugified, p3.pk),
            ]
        )

        expected = {
            p1.full_name_slugified: sorted([p2.pk, p3.pk]),
            p2.full_name_slugified: sorted([p1.pk, p3.pk]),
            p3.full_name_slugified: sorted([p1.pk, p2.pk]),
        }
        self.assertEqual(got, expected)

    def test_can_fetch_pks_weth_extra_kwarg(self):
        p1 = UserProfile.objects.get(user__email='fake1@email.com')
        p2 = UserProfile.objects.get(user__email='fake2@email.com')
        p3 = UserProfile.objects.get(user__email='fake3@email.com')

        got = check_user_fullname_pk_in_redis(
            [
                (p1.full_name_slugified, p1.pk),
                (p2.full_name_slugified, p2.pk),
                (p3.full_name_slugified, p3.pk),
            ],
            drop_self=False,
        )

        expected = {
            p1.full_name_slugified: sorted([p1.pk, p2.pk, p3.pk]),
            p2.full_name_slugified: sorted([p1.pk, p2.pk, p3.pk]),
            p3.full_name_slugified: sorted([p1.pk, p2.pk, p3.pk]),
        }
        self.assertEqual(got, expected)

    def tearDown(self):
        for email_login in ['fake1', 'fake2', 'fake3']:
            profile = UserProfile.objects.get(user__email=f'{email_login}@email.com')
            delete_user_fullname_in_redis(profile.full_name_slugified, profile.user.pk)
