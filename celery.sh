#!/bin/sh

if [ -f ".env" ]; then
    set -o allexport
    source .env
    set +o allexport
fi

export DJANGO_SETTINGS_MODULE=intake_platform.intake_platform.settings

celery -A intake_platform.intake_platform worker -l INFO
